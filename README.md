# BeerNest

## BeerNest is an online beer store project

this project is an assignment for 3-6th weeks of Neobis Club

## Project Information

### Technologies

This project was created and evolved using technologies like:

1) Java 21
2) Maven
3) Spring Boot
4) Spring Security
5) Oauth2 Resource Server
6) Spring Data JPA
7) PostgreSQL
8) openssl
9) Lombok
10) Postman
11) Swagger

...

you can find the full list of dependencies in POM.xml file

### Description

This is a small pet project for online-store. It retrieves products from database, shows how much of each product
it has and simulates a warehouse. To make an order you should be signed in, after Order was made Application refreshes
the amount of product available. Also it has two kind of Controllers: one for admin users, second is for customers,
and they are restricted by authorization accordingly.

### Installation

### Testing

To run tests just open terminal in root directory and type `mvn clean test`
### How To Use

`author` Yerbolat

`linkedIn` https://lnkd.in/ddpDGKY2

`date` Creation date: 04 January 2024
